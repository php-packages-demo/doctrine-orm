<?php
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

// replace with file to your own project bootstrap
if (!include_once __DIR__ . '/vendor/autoload.php'):
    require '/usr/share/php/Doctrine/Common/autoload.php';
    require '/usr/share/php/Doctrine/Common/Annotations/autoload.php';
    // require '/usr/share/php/Doctrine/Common/Persistence/autoload.php';
    require '/usr/share/php/Doctrine/Persistence/autoload.php';
endif;
// replace with mechanism to retrieve EntityManager in your app
// $entityManager = GetEntityManager();

return ConsoleRunner::createHelperSet(
    EntityManager::create(
        ['driver' => 'pdo_sqlite', 'memory' => true],
        // /path/to/entity/mapping/files
        Setup::createAnnotationMetadataConfiguration([('.')])
    )
);

// https://www.doctrine-project.org/api/orm/latest/Doctrine/ORM/EntityManager.html
// https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/tutorials/getting-started.html
