# doctrine/[orm](https://phppackages.org/p/doctrine/orm)

[**doctrine/orm**](https://packagist.org/packages/doctrine/orm)
![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/orm)
PHP object relational mapper (ORM)
https://www.doctrine-project.org

(Unofficial demo and howto)

* https://libraries.io/packagist/doctrine%2Form

[[_TOC_]]

# Documentation
## General
* [*Relational Database Design*
  ](https://en.wikibooks.org/wiki/Relational_Database_Design)

## Official documentation
### Tutorials
* [*Getting Started with Doctrine*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/getting-started.html)

### Reference
* [*Best Practices*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/best-practices.html)
* [*Tools*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/tools.html)

### Others
* [*Doctrine 2 ORM Best Practices*
  ](https://ocramius.github.io/doctrine-best-practices/#/)

### Events
* [*Doctrine Events*
  ](https://symfony.com/doc/current/doctrine/events.html)

### Second level cache
* [*The Second Level Cache*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/second-level-cache.html)

### Native SQL
* [*Native SQL*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/native-sql.html)

## Mixed documentation
### Data Definition Language (DDL)
* [doctrine orm relationship](https://google.com/search?q=doctrine+orm+relationship)
* [*Association Mapping*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/association-mapping.html)
* [*Association Updates: Owning Side and Inverse Side*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/unitofwork-associations.html)
* [*Working with Associations*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/working-with-associations.html)
* [*How to Work with Doctrine Associations / Relations*
  ](https://symfony.com/doc/master/doctrine/associations.html)
* [*Mastering Doctrine Relations!*
  ](https://symfonycasts.com/screencast/doctrine-relations)
* (fr) [*Relation ManyToOne et OneToMany - 1..n*
  ](https://zestedesavoir.com/tutoriels/1713/doctrine-2-a-lassaut-de-lorm-phare-de-php/les-relations-avec-doctrine-2/relation-manytoone-et-onetomany-1-n/)
  2019-01 BestCoder

### Doctrine\ORM\Tools\Pagination
* [*Pagination*
  ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/pagination.html)
* (fr) [*Symfony — Paginer les résultats d’une requête avec Doctrine*
  ](https://nicolasfz-code.medium.com/symfony-paginer-les-r%C3%A9sultats-dune-requ%C3%AAte-avec-doctrine-ebe7873197c9)
  2020-12 NicolasFz.code

## Unofficial documentation
* [doctrine orm](https://google.com/search?q=doctrine+orm)
* [doctrine entities](https://google.com/search?q=doctrine+entities)
* [doctrine entities](https://google.com/search?q=doctrine+entities)
* [designing doctrine entities](https://google.com/search?q=designing+doctrine+entities)
---
* [*Symfony UUID, Doctrine and type-hinting: everything you should know*
  ](https://medium.com/ekino-france/symfony-uuid-doctrine-and-type-hinting-everything-you-should-know-b846519a9927)
  And why you should look at how everything work under the hood
  2023-05 Nicolas Nénon (ekino-france)

### Introduction
* [*Doctrine for Dummies: 50 Recommendations to Optimize ORM and Turbocharge Your App 🚗💨*
  ](https://medium.com/@ahmedbhs/doctrine-for-dummies-50-recommendations-to-optimize-orm-and-turbocharge-your-app-4a7e7091544b)
  2024-11 Ahmed EBEN HASSINE (Medium)
* [*Mastering Doctrine ORM relations*
  ](https://medium.marco.zone/mastering-doctrine-orm-relations-571060c5b40e)
  2021-11 Marco Pfeiffer
* *Let’s Look Under the Hood of Doctrine 2*
  * [*Let’s Look Under the Hood of Doctrine 2*
    ](https://stfalcon.com/en/blog/post/doctrine2-ORM-architecture)
    2018 [Yevhenii Vaskevych](https://stfalcon.com/en/blog/author/yevhenii%20vaskevych)
    @stfalcon.com
  * [*Let’s Look Under the Hood of Doctrine 2*
    ](https://codeburst.io/lets-look-under-the-hood-of-doctrine-2-7861695a2de6?gi=105e1d58a37f)
    2018 [Yevhenii Vaskevych](https://stfalcon.com/en/blog/author/yevhenii%20vaskevych)
    @codeburst.io

### Referential integrity and cascade deletion
* [*Database referential integrity with Doctrine*
  ](https://dev.to/altesack/database-referential-integrity-with-doctrine-1539)
  2024-05 Marat Latypov (DEV)
* [*How to delete… not to delete yourself?*
  ](https://medium.com/accesto/how-to-delete-not-to-delete-yourself-accesto-blog-9c252f684db5)
  2021-08 Michał Romańczuk
* [*Cascade Persist & Remove in Doctrine*
  ](https://medium.com/@biberogluyusuf/cascade-persist-remove-in-doctrine-559ac967e451)
  2020-09 Yusuf Biberoğlu

### DQL
* [*Doctrine — DQL functions cheat-sheet*
  ](https://medium.com/geekculture/doctrine-dql-functions-cheat-sheet-bfcb3f3ff45e)
  2021-08 Alexandre Daubois (Medium)

### Entity Manager
* [*How to Deal with a Closed Entity Manager in Doctrine*
  ](https://kisztof.medium.com/how-to-deal-with-a-closed-entity-manager-in-doctrine-ac3a2c1d74a1)
  2023-10 Krzysztof Słomka (Medium)

### Events
* [*Symfony 4 – Doctrine Event Subscriber*
  ](https://blog.dev-web.io/2019/11/06/symfony-4-doctrine-event-subscriber/)
  2019-11

### SQL views
* [*Aggregate your data by using SQL views and Doctrine*
  ](https://robbert.rocks/aggregate-your-data-by-using-sql-views-and-doctrine)
  2020-07 robbert.rocks

### Traits
* [*Doctrine+Symfony: adding indexes to fields defined in traits*
  ](https://medium.com/@alexkunin/doctrine-symfony-adding-indexes-to-fields-defined-in-traits-a8e480af66b2)
  2019-07 Alex Kunin
* [*Using traits to compose your Doctrine entities*
  ](https://medium.com/@galopintitouan/using-traits-to-compose-your-doctrine-entities-9b516335119b)
  2018-08 Titouan Galopin
* [*Optimizing Doctine2 entities with Traits*
  ](https://medium.com/@ivanproskuryakov/optimizing-doctine2-entities-with-traits-9c7b38247f46)
  2015-09 .. 2019-09 Ivan Proskuryakov

### Two databases
* [*Two databases with Symfony 4 & Doctrine 2? Not as straightforward as it might look*
  ](https://medium.com/@raito.akehanareru/two-databases-with-symfony-4-doctrine-2-not-as-straightforward-as-it-might-look-906e57cee710)
  2019-08 Martin Jagr

### Loosely coupled application components architectures
#### Domain Events
* [*Decouple your Symfony application using Domain Events*
  ](https://romaricdrigon.github.io/2019/08/09/domain-events)
  2019-08 Romaric Drigon

#### Hexagonal architecture
* (fr) [*Simplifiez vos formulaires Symfony*](https://devblog.lexik.fr/symfony/simplifiez-vos-formulaires-symfony-3276)
  2019-09 Gilles

#### Value objects in Domain-driven design
* [*Stop repeating yourself in Doctrine and Symfony forms with Value objects*
  ](https://romaricdrigon.github.io/2019/07/05/value-objects-doctrine-and-symfony-forms)
  2019-07 [Romaric Drigon](https://romaricdrigon.github.io)
* [*ValueObject*](https://martinfowler.com/bliki/ValueObject.html)
  2016-11 [Martin Fowler](https://martinfowler.com/)
* [*Value Objects, Immutable objects and Doctrine Embeddables*
  ](https://io.serendipityhq.com/experience/php-and-doctrine-immutable-objects-value-objects-and-embeddables/)
  2015-07 Aerendir
* [*Value Object*](http://wiki.c2.com/?ValueObject)
* [*Value object*](https://en.m.wikipedia.org/wiki/Value_object)
* [*Domain-driven design*](https://en.m.wikipedia.org/wiki/Domain-driven_design)

# Related projects
## Projects documenting this one
* [hub-docker-com-demo/php](https://gitlab.com/hub-docker-com-demo/php)

# Package
* [php-doctrine-orm](https://packages.debian.org/stable/php-doctrine-orm)@Debian

## Popularity (Debian)
![PHP&Java ORM](https://qa.debian.org/cgi-bin/popcon-png?packages=php-doctrine-orm+libspring-orm-java+libhibernate3-java&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2006-01-01&to_date=&hlght_date=&date_fmt=%25Y)

![ORM](https://qa.debian.org/cgi-bin/popcon-png?packages=php-doctrine-orm+libspring-orm-java+libhibernate3-java+python3-sqlalchemy+python-sqlalchemy+python3-tables+python-tables&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2006-01-01&to_date=&hlght_date=&date_fmt=%25Y)

* ![doctrine/orm Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/orm) doctrine/orm
![sqlalchemy PyPI - Downloads](https://img.shields.io/pypi/dm/sqlalchemy) sqlalchemy
* ![sqlalchemy pepy.tech Downloads](https://static.pepy.tech/badge/sqlalchemy/month) sqlalchemy
